<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;

use Illuminate\Http\Request;
use App\Http\Requests\UsersRequest as FormRequest;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate();
        return View('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get()->pluck('name','id');
        return View('users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormRequest $request)
    {
        $user = User::create($request->all());
        if($user) 
        {
            $response = [
                'status'    => 201,
                'message'   => 'Data has been stored to database',
                'redirect'  => route('users.index')
            ];
        }
        else
        {
            $response = [
                'status'    => 400,
                'message'   => 'Data has been failed to database',
                'redirect'  => route('users.index')
            ];
        }

        return response()->json($response,$response['status']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return View('users.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if(!$user) 
            abort(404);

        $roles = Role::get()->pluck('name','id');
        return View('users.edit',compact('roles','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormRequest $request,  $id)
    {
        $user = User::find($id);
        if(!$user) 
        {
            $response = [
                'status'    => 404,
                'message'   => 'user not found in database',
                'redirect'  => route('users.index')
            ];
            return response()->json($response,$response['status']);
        }

        $user->update($request->all());
        if($user) 
        {
            $response = [
                'status'    => 201,
                'message'   => 'Data has been updated to database',
                'redirect'  => route('users.index')
            ];
        }
        else
        {
            $response = [
                'status'    => 400,
                'message'   => 'Data has been failed to database',
                'redirect'  => route('users.index')
            ];
        }

        return response()->json($response,$response['status']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if(!$user) 
        {
            $response = [
                'status'    => 404,
                'message'   => 'user not found in database',
                'redirect'  => route('users.index')
            ];
            return response()->json($response,$response['status']);
        }

        $user->delete();
        if($user) 
        {
            $response = [
                'status'    => 200,
                'message'   => 'Data has been deleted to database',
                'redirect'  => route('users.index')
            ];
        }
        else
        {
            $response = [
                'status'    => 400,
                'message'   => 'Data has been failed to delete',
                'redirect'  => route('users.index')
            ];
        }

        return response()->json($response,$response['status']);
    }
}
