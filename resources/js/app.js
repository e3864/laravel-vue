/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require("./bootstrap");

 window.Vue = require("vue").default;
 
 /**
	* The following block of code may be used to automatically register your
	* Vue components. It will recursively scan this directory for the Vue
	* components and automatically register them with their "basename".
	*
	* Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
	*/
 
 // const files = require.context('./', true, /\.vue$/i)
 // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
 
 //Vue.component('example-component', require('./components/ExampleComponent.vue').default);
 
 /**
	* Next, we will create a fresh Vue application instance and attach it to
	* the page. Then, you may begin adding components to this application
	* or customize the JavaScript scaffolding to fit your unique needs.
	*/

import toastr from 'toastr';
 
 const app = new Vue({
	 el: "#app",

	 mixins: [
		//Global
	 ],

	 mounted() {
		 console.log("mounted");
	 },
	 methods: {
		onDelete: function (event) {
			if (! confirm('Are you sure you want to delete ')) {
				return;
			} 

			const url = event.target.getAttribute('data-href');

			axios({
				method: 'DELETE',
				url: url
			})
				.then(function (response) {
					if(response.data.status == 200) {
						toastr.success(response.data.message);
					} else {
						toastr.error(response.data.message);
					}
					window.location.href = response.data.redirect;
			})
				.catch(function(error) {

				});
			
		},
		onSubmit: function (event) {
			const url = event.target.action;
			const method = event.target.method;
			const formId = event.target.id;
			const formData = new FormData(document.getElementById(formId));
			
			axios({
				method: method,
				url: url,
				data: formData
			})
				.then(function (response) {
					console.log(response.data);
					if(response.data.status == 200 || response.data.status == 201) {
						toastr.success(response.data.message);
					} else {
						toastr.error(response.data.message);
					}
					window.location.href = response.data.redirect;
			})
				.catch(function(error) {
					const status = error.response.status;
					switch(status) {
						case 422: 
							const errors = error.response.data.errors;
							const errJson = JSON.stringify(errors);
							const obj = JSON.parse(errJson);
							console.log('errors',obj);
							$.each(errors, function( index, value ) {
								console.log('value:',value[0]);
								toastr.error(value[0]);
							});
							break;
					} 

				});

		 },
	 },
 });
 