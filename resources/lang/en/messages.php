<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during messages for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'saved' => 'Your data has been saved.',
    'deleted' => 'Your data has been deleted.',
    'updated' => 'Your data has been updated',

];
