<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Main CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" media="screen" >

    <script type="text/javascript"><!--
      window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
      ]) !!};
  //--></script>

    <title>@yield('title')</title>
  </head>
  <body>
    
    <div id="app">
        @yield('content')
    </div>

    <script src="{{ asset('js/app.js') }}" ></script>
  </body>
</html>