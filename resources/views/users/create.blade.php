@extends('layouts.main')
 
@section('title', 'Page Title')
 
@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! 
            Form::open([
                'route' => 'users.store',
                'id' => 'form',
                '@submit.prevent' => 'onSubmit',
                'novalidate' => true,
                'role' => 'form',
            ]) !!}

                {{ Form::label('name', 'Name') }}
                {{ Form::text('name','',['class' => 'form-control']) }}

                {{ Form::label('email', 'Email') }}
                {{ Form::email('email','',['class' => 'form-control']) }}

                {{ Form::label('role_id', 'Role') }}
                {{ Form::select('role_id', $roles,'',['class' => 'form-control']) }}

                {{ Form::label('password', 'Password') }}
                {{ Form::password('password',['class' => 'form-control']) }}

                {{ Form::submit('Submit') }}

            {!! Form::close() !!}
        </div>
    </div>
@endsection