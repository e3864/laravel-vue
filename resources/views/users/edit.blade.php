@extends('layouts.main')
 
@section('title', 'Page Title')
 
@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! 
            Form::model($user,[
                'route' => ['users.update','id' => $user->id],
                'method' => 'PUT',
                'id' => 'form',
                '@submit.prevent' => 'onSubmit',
                'novalidate' => true,
                'role' => 'form',
            ]) !!}

                {{ Form::label('name', 'Name') }}
                {{ Form::text('name',$user->name,['class' => 'form-control']) }}

                {{ Form::label('email', 'Email') }}
                {{ Form::email('email',$user->email,['class' => 'form-control']) }}

                {{ Form::label('role_id', 'Role') }}
                {{ Form::select('role_id', $roles,$user->role_id,['class' => 'form-control']) }}

                {{ Form::label('password', 'Password') }}
                {{ Form::password('password',['class' => 'form-control']) }}

                {{ Form::submit('Submit') }}

            {!! Form::close() !!}
        </div>
    </div>
@endsection