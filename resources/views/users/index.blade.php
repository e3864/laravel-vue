@extends('layouts.main')
 
@section('title', 'Page Title')
 
@section('content')
    <a href="{{ route('users.create') }}">Add New</a>
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->role->name }}</td>
                    <td>
                        <a href="{{ route('users.edit',[$item->id]) }}">Edit</a> 
                        | <a v-on:click="onDelete" href="#" data-href="{{ route('users.destroy',['id' => $item->id]) }}">Destroy</a> 
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection